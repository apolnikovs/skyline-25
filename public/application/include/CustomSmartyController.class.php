<?php

/**
 * Short Description of CustomSmartyController.
 * 
 * Long description of CustomSmartyController.
 *
 * @author     Brian Etherington <brian@smokingun.co.uk>
 * @copyright  2011 Smokingun Graphics
 * @link       http://www.smokingun.co.uk
 * @version    1.1
 *  
 * Changes
 * Date        Version Author                Reason
 * 23/01/2012  1.0     Brian Etherington     Initial Version 
 * 09/11/2012  1.1     Brian Etherington     use setters introduced Smarty V3.1  
 * 13/05/2013  1.2     Andris Polnikovs      Added print_d fucntion   
 ******************************************************************************/

require_once('CustomController.class.php');
require_once(APPLICATION_PATH . '/smarty/libs/Smarty.class.php');

abstract class CustomSmartyController extends CustomController {
    
    // beginning of docblock template area
    /**#@+
     * @access protected
     */
    protected $smarty;
    /**#@-*/
    
    // beginning of docblock template area
    /**#@+
     * @access private
     */
    //  private vars here...
    /**#@-*/
    
    /**
     * Short Description.
     * 
     * Description.
     * 
     */
    public function __construct() {
        
        parent::__construct();
        
        $this->smarty = new Smarty();
        
        // use setters introduced Smarty V3.1
        $this->smarty->setCompileDir( APPLICATION_PATH . '/smarty/template_c/' );
        $this->smarty->setConfigDir( APPLICATION_PATH . '/smarty/configs/' );
        $this->smarty->setCacheDir( APPLICATION_PATH . '/smarty/cache/' );
        $this->smarty->setTemplateDir( APPLICATION_PATH . '/views/' );
        $this->smarty->addPluginsDir( APPLICATION_PATH . '/smarty/plugins/' );

        //$this->smarty->compile_dir  = APPLICATION_PATH . '/smarty/template_c/';
        //$this->smarty->config_dir   = APPLICATION_PATH . '/smarty/configs/';
        //$this->smarty->cache_dir    = APPLICATION_PATH . '/smarty/cache/';
        //$this->smarty->addPluginsDir( APPLICATION_PATH . '/smarty/plugins/' );
        //$this->smarty->template_dir = APPLICATION_PATH . '/views/';

        //$this->smarty->caching = Smarty::CACHING_LIFETIME_CURRENT;
        $this->smarty->caching = Smarty::CACHING_OFF;
        
        if (defined('SUB_DOMAIN')) {
            $this->smarty->assign('_subdomain', SUB_DOMAIN);
        } else {
            $this->smarty->assign('_subdomain', '');
        }
        
        /* ==========================================
         * initialise response headers
         * ==========================================
         */
        
        header('Content-type: text/html; charset=utf-8');
	header("Cache-Control: no-cache, must-revalidate");
	header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
        /* Add this X-UA-COMPATIBLE header to avoid this error in IE
         * even with a valid <!DOCTYPE at the start of http response content
         * IE9 still throws up this exasperating error!!!!!!!!
         * Note: IE=edge means use the most up to date rendering for the current browser
         * that is, IE7 will render as IE7, IE8 as IE8 and IE9 as IE9
         * this is MS craziness...
         * HTML1113: Document mode restart from Quirks to IE9 Standards */
        header('X-UA-Compatible: IE=edge');
        /*
         * privacy policy to keep IE happy with third party content (ie Skyline) in IFrames
         */
        header('P3P:CP="IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT"');

    }
    //displays data array in readable format
    public function print_d($d){
        echo"<pre>";
            print_r($d);
        echo"</pre>";
    }
   

    

   
}

?>
