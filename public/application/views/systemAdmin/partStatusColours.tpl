{extends "DemoLayout.tpl"}


{block name=config}
    {$Title = $page['Text']['page_title']|escape:'html'}
    {$PageId = $PartStatusColourPage}
    {$fullscreen=true}
{/block}
{block name=afterJqueryUI}
    <script type="text/javascript" src="{$_subdomain}/js/jquery.combobox.js"></script>
    <script type="text/javascript" src="{$_subdomain}/js/TableTools.min.js"></script>
    <script type="text/javascript" src="{$_subdomain}/js/datatables.api.js"></script>
    <link rel="stylesheet" href="{$_subdomain}/css/themes/pccs/style.css" type="text/css" media="screen" charset="utf-8" />
{/block}

{block name=scripts}


   
 <script type="text/javascript" src="{$_subdomain}/js/ColReorder.js"></script>
    <script type="text/javascript">
         var $statuses = [
                    {foreach from=$statuses item=st}
                       ["{$st.Name}", "{$st.Code}"],
                    {/foreach}
                    ]; 
     
                 
    
                    
        
    function gotoEditPage($sRow)
    {
        
         
        $('#updateButtonId').removeAttr('disabled').removeClass('gplus-blue-disabled').addClass('gplus-blue');
        $('#updateButtonId').trigger('click');
       
       
    }
function createColourPicker(nRow, aData){
 
  
                $('td:eq(3)', nRow).html( '<div style="width:60px;background-color:#'+aData[3]+';color:#'+aData[3]+';" >&nbsp;</div>' );
        
        
        
       
    
}
      
 $(document).ready(function() {

 var oTable = $('#PartStatusColourResults').dataTable( {
"sDom": 'R<"left"><"top"f>t<"bottom"><"centered"><"right">pli<"clear">',
//"sDom": 'Rlfrtip',

"bServerSide": true,

		fnRowCallback:  function(nRow, aData){
               createColourPicker(nRow, aData);
                
                },
    "sAjaxSource": "{$_subdomain}/LookupTables/loadPartStatusColourTable",
     
                "fnServerData": function ( sSource, aoData, fnCallback ) {
			/* Add some extra data to the sender */
			aoData.push( { "name": "more_data", "value": "my_value" } );
			$.getJSON( sSource, aoData, function (json) { 
				/* Do whatever additional processing you want on the callback, then tell DataTables */
				fnCallback(json)
                                $('#tt-Loader').hide();
                                $('#PartStatusColourResults').show();
                               
			} );
                        },
                        
"bPaginate": true,
"sPaginationType": "full_numbers",
"aLengthMenu": [[ 25, 50, 100 , -1], [25, 50, 100, "All"]],
"iDisplayLength" : 25,
 "aoColumns": [ 
			
			
			{for $er=0 to $data_keys|@count-1}
                                {$vis=1}
                               
                               
                           {if $er==3} 
                           { "bVisible":{$vis},"sWidth":60 },
                            {else}
                            { "bVisible":{$vis} },
                                {/if}
                           {/for} 
                              
                               
                            
		] 
   
 
        
          
});//datatable end
  
   /* Add a click handler to the rows - this could be used as a callback */
	$("#PartStatusColourResults tbody").click(function(event) {
		$(oTable.fnSettings().aoData).each(function (){
			$(this.nTr).removeClass('row_selected');
		});
		$(event.target.parentNode).addClass('row_selected');
                var anSelected = fnGetSelected( oTable );
                var aData = oTable.fnGetData(anSelected[0]); // get datarow
                
    if (anSelected!="")  // null if we clicked on title row
    {
    
                $('#exportType').val(aData[0]);
                }
	});

 /* Get the rows which are currently selected */
function fnGetSelected( oTableLocal )
{
	var aReturn = new Array();
	var aTrs = oTableLocal.fnGetNodes();
	
	for ( var i=0 ; i<aTrs.length ; i++ )
	{
		if ( $(aTrs[i]).hasClass('row_selected') )
		{
			aReturn.push( aTrs[i] );
		}
	}
	return aReturn;
}



    /* Add a click handler for the edit row */
	$('button[id^=edit]').click( function() {
		var anSelected = fnGetSelected( oTable );
                var aData = oTable.fnGetData(anSelected[0]); // get datarow
    if (anSelected!="")  // null if we clicked on title row
    {
        console.log(anSelected);
  //becouse all the data can be reordered and hidden any additinal info like ID needs to be stored in <tr> and accesed by anSelected[0].id where .id is data needet
$.colorbox({ 
 
                           href:"{$_subdomain}/LookupTables/processPartStatusColour/id="+anSelected[0].id,
                        title: "Edit PartStatusColour",
                        opacity: 0.75,
                        width:800,
                        overlayClose: false,
                        escKey: false

                });
    }else{
    alert("Please select row first");
    }
		
	} );                            
             
 

                        
             

/* Add a dblclick handler to the rows - this could be used as a callback */
	$("#PartStatusColourResults  tbody").dblclick(function(event) {
		$(oTable.fnSettings().aoData).each(function (){
			$(this.nTr).removeClass('row_selected');
		});
		$(event.target.parentNode).addClass('row_selected');
            var anSelected = fnGetSelected( oTable );
                var aData = oTable.fnGetData(anSelected[0]); // get datarow
    if (anSelected!="")  // null if we clicked on title row
    {
    
$.colorbox({ 
 
                        href:"{$_subdomain}/LookupTables/processPartStatusColour/id="+anSelected[0].id,
                        title: "Edit Part Status Colour",
                        opacity: 0.75,
                        width:800,
                        overlayClose: false,
                        escKey: false

                });
    }else{
    alert("Please select row first");
    }
		
	} );  




      
        $('#manufacturerFilter').combobox();
        $('#serviceProviderSelect').combobox({
        selected: function( event, ui ) {
   
    $('#serviceProviderSelect').trigger("change");                            
}
        });


    });//doc ready end

function PartStatusColourEdit()
{
    var anSelected = fnGetSelected( oTable );
                var aData = oTable.fnGetData(anSelected[0]); 
               
$.colorbox({ 
 
                        href:"{$_subdomain}/LookupTables/processPartStatusColour/id="+aData,
                        title: "Edit Part Fault Codes",
                        opacity: 0.75,
                        width:800,
                        overlayClose: false,
                        escKey: false

                });

}


                    
                    
                
                
             
function filterTable(name){
$('input[type="text"]','#PartStatusColourResults_filter').val(name);
e = jQuery.Event("keyup");
e.which = 13;
$('input[type="text"]','#PartStatusColourResults_filter').trigger(e);


}



    </script>

    
{/block}


{block name=body}

    <div class="breadcrumb" style="width:100%">
        <div>

            <a href="{$_subdomain}/SystemAdmin" >{$page['Text']['system_admin_home_page']|escape:'html'}</a> / <a href="{$_subdomain}/SystemAdmin/index/lookupTables" >Lookup Tables</a> {if isset($backtomodel)}/<a href="{$_subdomain}SystemAdmin/index/lookupTables/models" > Models</a> {/if}/ Part Status Colours

        </div>
    </div>



    <div class="main" id="home" style="width:100%">

               <div class="ServiceAdminTopPanel" >
                    <form id="PartStatusColourTopForm" name="PartStatusColourTopForm" method="post"  action="#" class="inline">
                         
                        <fieldset>
                        <legend title="" >Part Status Colour</legend>
                        <p>
                            <label>This menu option will allow a user to define a colour coding against each Part Status</label>
                        </p> 

                        </fieldset> 


                    </form>
                </div>  


                  <div class="ServiceAdminResultsPanel" id="PartStatusColourResultsPanel" >
                    
                  
                 
                     <div style="text-align:center" id="tt-Loader"><img src="{$_subdomain}/images/ajax-loading_medium.gif"></div>
                      
                     
                        
                    <form method="POST" action="" id="PartStatusColourResultsForm" class="dataTableCorrections">
                        <table style="display:none" id="PartStatusColourResults" border="0" cellpadding="0" cellspacing="0" class="browse" >
                            <thead>
                                    <tr>
                                            
                                        {foreach from=$data_keys key=kk item=vv}
                                  
                                    <th>
                                        {$vv}
                                    </th>
                                  
                                {/foreach}
                           
                                
                            </thead>
                            <tbody>
                              
                            
                            </tbody>
                        </table>  
                           
                           
                     </form>
                </div>        

                  <div class="bottomButtonsPanelHolder" style="position: relative;">
                    <div class="bottomButtonsPanel" style="position:absolute;top:-65px;width:200px">
                            <button type="button" id="edit"  class="gplus-blue">Edit</button>
                          
                            
                    </div>
                </div>  
                 <div class="centerInfoText" id="centerInfoText" style="display:none;" ></div>
               

                 
                 

<hr>
                <button class="gplus-blue" style="float:right" type="button" onclick="window.location='{$_subdomain}/SystemAdmin/index/lookupTables'">Finish</button>
    
    </div>
                 



{/block}



