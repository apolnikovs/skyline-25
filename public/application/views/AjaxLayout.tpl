
{* ************************************************************************

This template is for responding with Ajax content using the same 
SMARTY blocks as IframeLayout.

In this way the same template can be used to respond with either a full HTML
page as an Iframe using IFrameLaout.tpl or as ajax content using
AjaxLayout.tpl, simply by changing the extends filename in the page layout file.

*************************************************************************** *}

<div>
{block name=body}{/block}
</div>

<script>
{block name=scripts}{/block}
</script>