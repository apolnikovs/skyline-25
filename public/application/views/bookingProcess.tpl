{extends "DemoLayout.tpl"}


{block name=config}
    {$Title = $page['Text']['page_title']|escape:'html'}
    {$PageId = $JobBookingProcessPage}
{/block}


{block name=afterJqueryUI}
   <script type="text/javascript" src="{$_subdomain}/js/jquery.combobox.js"></script>
   <script type="text/javascript" src="{$_subdomain}/js/TableTools.min.js"></script>
   <link rel="stylesheet" href="{$_subdomain}/css/themes/pccs/style.css" type="text/css" media="screen" charset="utf-8" /> 

<style>
    .ui-combobox-input {
	 width:300px;
     }  

    .ui-icon { padding: 0; }
</style>

{/block}


{block name=scripts}

    
{*<link rel="stylesheet" href="{$_subdomain}/css/colorbox/colorbox.css" type="text/css" charset="utf-8" />*} 
{*<script type="text/javascript" src="{$_subdomain}/js/jquery.jlabel-1.3.min.js"></script>*} 
{*<script type="text/javascript" src="{$_subdomain}/js/jquery.form.min.js"></script>*}
{*<script type="text/javascript" src="{$_subdomain}/js/jquery.dataTablesPCCS.js"></script>*}
       
 
<script type="text/javascript">
      
    $(document).ready(function() {
      
	$("#CourierID").combobox();
      
	/* Add a click handler to the 'Finish' button of Booking Process Page - strats here*/
        $(document).on('click', '#job_booking_finish_btn', 
            function() {


                    var url = "{$_subdomain}/index/";    
                    $(location).attr('href',url);


                    return false;

            }      
        );
        /* Add a click handler to the 'Finish' button of Booking Process Page - ends here*/
        
        
        /* Add a click handler to the 'Open Job Record' button of Booking Process Page - strats here*/
        $(document).on('click', '#open_job_record_btn', 
            function() {


                    var url = "{$_subdomain}/index/jobupdate/"+urlencode("{$bp_number|escape:'html'}");    
                    $(location).attr('href',url);


                    return false;

            }      
        );
        /* Add a click handler to the 'Open Job Record' button of Booking Process Page - ends here*/
        
        
        /* Add a click handler to the 'Print Job Record' button of Booking Process Page - starts here */
        $(document).on('click', '#print_job_record_btn', function() {
	    var url = "{$_subdomain}/Job/printJobRecord/" + urlencode("{$bp_number|escape:'html'}") + "/1";
	    var newtab = window.open();
	    newtab.location = url;
	    return false;
        });
        /* Add a click handler to the 'Print Job Record' button of Booking Process Page - ends here*/
        
        
        /* Add a click handler to the 'Print Customer Receipt' button of Booking Process Page - starts here*/
        $(document).on('click', '#print_customer_receipt_btn', function() {
	    var url = "{$_subdomain}/Job/printJobRecord/" + urlencode("{$bp_number|escape:'html'}") + "/2";
	    var newtab = window.open();
	    newtab.location = url;
	    return false;
        });
        /* Add a click handler to the 'Print Customer Receipt' button of Booking Process Page - ends here*/
        
	
        $(document).on('click', '#print_btn', function() {
	    openPrintModal();
	    return false;
        });


	function openPrintModal() {
    
	    var html = "<fieldset>\
			    <legend align='center'>Print Options</legend>\
			    <br/>\
			    <input name='printRadio' type='radio' value ='2' /> Print Customer Receipt\
			    <br/>\
			    <input name='printRadio' type='radio' value ='1' /> Print Job Card\
			    <br/>\
			    <input name='printRadio' type='radio' value ='3' /> Print Service Report\
			    <br/>\
			    <br/>\
			    <div style='width:100%; text-align:center;'>\
				<a href='#' id='print' style='width:50px;' class='DTTT_button'>Print</a>&nbsp;\
				<a href='#' id='cancel' style='width:50px;' class='DTTT_button'>Cancel</a>\
			    </div>\
			</fieldset>\
		       ";

	    $.colorbox({
		html :	html, 
		width:	"320px",
		scrolling:	false
	    });

	    $(document).on("click", "#print", function() {
		var printType = $("input[name=printRadio]:radio:checked").val();
		$.colorbox.close();
		var url = "{$_subdomain}/Job/printJobRecord/{$bp_number}/" + printType + "/?pdf=1";
		$("#print").die();
		window.open(url, "_blank");
		window.focus();
		return false;
	    });
	
	}
        
	$(document).on("click", "#cancel", function() {
	    $.colorbox.close();
	    return false;
	});
        
        {********************** Book Collection Handling *****************}
        $(document).on('click', '#BookCollectionButton', 
            function() {
                $.colorbox({
                    href :	'{$_subdomain}/Courier/BookCollectionForm/job={$bp_number}/from_job_booking=true', 
                    width:	'700px',
                    scrolling:	false,
                    overlayClose: false,
                    fixed: true
                });
            });
        {********************** Book Collection End **********************}

  {*      
        $(document).on('click', "#update_save_btn", function() { 

                      $("#update_save_btn").hide();
                      $("#cancel_btn").hide();
                      $("#processDisplayText").show();
                      
                      
                      
                      $('#CourierPreferencesForm').validate({
                                         
                                         
                        ignore: '',

                        rules:  {

                                 CollectionDate:
                                {
                                   dateITA: true
                                }

                           },
                        messages: {
                                    CollectionDate:
                                    {
                                        dateITA: "{$page['Errors']['collection_date']|escape:'html'}"
                                    }
                         }, 
                        errorPlacement: function(error, element) {

                            
                             error.insertAfter( element );
                             
                                $("#update_save_btn").show();
                                $("#cancel_btn").show();
                                $("#processDisplayText").hide();

                        },
                        errorClass: 'fieldError',
                        onkeyup: false,
                        onblur: false,
                        errorElement: 'label',

                        submitHandler: function() {


                                $.post("{$_subdomain}/Job/updateJobCourierPreference/",        

                                $("#CourierPreferencesForm").serialize(),      
                                function(data){

                                     document.location.href = "{$_subdomain}/Job/confirmed/"+urlencode("{$bp_number|escape:'html'}");


                                }); //Post ends here...

                            }
                        });
                      
                      
                      

                      


              }); 
      
   *}   
  {*    
        
        $(document).on('click', "#cancel_btn", function() { 

          //$.colorbox.close();

          $("#update_save_btn").hide();
          $("#cancel_btn").hide();
          $("#processDisplayText").show();

           document.location.href = "{$_subdomain}/Job/confirmed/"+urlencode("{$bp_number|escape:'html'}");

         });  
  *}      
        
        
        {*if $service_center.CourierStatus eq 'Active'}
        
        $(document).on('click', '#CourierAppointmentElement', 
            function() {

                     //It opens color box popup page.              
                    $.colorbox( {   inline:true,
                                    href:"#DivCourierAppointment",
                                    title: '',
                                    opacity: 0.75,
                                    height:540,
                                    width:720,
                                    overlayClose: false,
                                    escKey: false,
                                    onLoad: function() {
                                       // $('#cboxClose').remove();
                                    },
                                    onClosed: function() {

                                       //location.href = "#EQ7";
                                    },
                                    onComplete: function()
                                    {
                                        $.colorbox.resize();
                                    }

                                 }); 
                   
                    return false;

            }      
        );
        
        {/if*}
        
        
         {if $service_center.OnlineDiary eq 'Active'}
             
         $(document).on('click', '#BookAppointmentElement', 
            function() {


                   window.open("{$_subdomain}/Job/bookAppointment/{$bp_number|escape:'html'}#home", 'TheNewpop', 'width=910px, height=600px, toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no'); 

                    return false;

            }      
        );
        
        
        
         $(document).on('click', '#BookAppointmentLink', 
            function() {


                 $('#BookAppointmentElement').trigger("click");
                  
                  return false;

            }      
        );
        
        {/if}
        
        
 {*       $( "#CollectionDate" ).datepicker({
			dateFormat: "dd/mm/yy",
                        showOn: "button",
			buttonImage: "{$_subdomain}/css/Skins/{$_theme}/images/calendar.png",
			buttonImageOnly: true,
                        minDate: '0',
                        changeMonth: true,
                        changeYear: true,
                        onClose: function(dateText, inst) { 
                        
                        if($("#CollectionDate").val()!="dd/mm/yyyy")
                        {    
                            $("#CollectionDate").removeClass("auto-hint"); 
                        }
                        
                        },
                        onSelect: function(dateText, inst) { 
                        
                        
                             
                        }
                    });
   *}     
        
        
      
      });
      
</script>

{/block}

{block name=body}

<div class="breadcrumb">
    <div>
        <a href="{$_subdomain}/index/index">{$page['Text']['home_page']|escape:'html'}</a> / {$page['Text']['job_booking']|escape:'html'} / {$page['Text']['booking_process']|escape:'html'}
    </div>
</div>
        
<div class="main" id="home">

    <div class="bookingProcessPanel"  >

         <form id="jobBookingProcessForm" name="jobBookingProcessForm" method="post"  action="#" class="inline">

            <fieldset>
            <legend title="GP7505" >{$page['Text']['legend']|escape:'html'}</legend>
            
            <div class="span-22 last">
                <div id="left-panel" class="span-4" style="text-align: center;">
                    <br />
                    {if $service_center.CourierStatus eq 'Active' or $AP10000}
                    <a id="BookCollectionButton" href="#">
                        <span style="text-align: center;display: inline-block;">
                            <img src="{$_subdomain}/css/Skins/{$_theme}/images/icons_skyline_truck_big.png" 
                                 alt="{$page['Text']['click_courier_appointment']|escape:'html'}" 
                                 title="{$page['Text']['click_courier_appointment']|escape:'html'}" 
                                 width="68" height="53"  />
                            <br />
                            {$page['Text']['click_courier_appointment']|escape:'html'}
                        </span>
                    </a>
                    {else}
                    <div id="BookAppointmentElement"  style="width: 145px; height: 150px; text-align:center;" ></div>
                    {/if}
                </div>
           
                <div id="center-panel" class="span-14">
                    <p>
                        <label>
                
                            {if $product_address}   
                            <span>{$page['Text']['product_address']|escape:'html'}: </span>
                            <br><br>
                            {$product_address|escape:'html'}
                            <br><br>                
                            {/if}    
                
                            <span>{$page['Text']['queries']|escape:'html'}<b>SL{$bp_number|escape:'html'}</b></span>
                            <br><br>
            
                            {if $service_center && $service_center|@count gt 0}
                            <span>
                            {$page['Text']['service_centre']|escape:'html'}:&nbsp;{$service_center.CompanyName|escape:'html'}&nbsp;&nbsp;{$page['Text']['telephone']|escape:'html'}:&nbsp;{$service_center.ContactWorkPhone|escape:'html'}&nbsp;&nbsp;{$page['Text']['email']|escape:'html'}:&nbsp;{$service_center.ContactEmail|escape:'html'}
                            </span>
                            <br><br>
                            <span>{$service_center.FullAddress|escape:'html'}</span>
                            <br>
                            <span>{$service_center.CompanyName|escape:'html'} {$page['Text']['job_no']|escape:'html'}: <b>
                    
                            {if $service_center.ServiceCentreJobNo}
                            SC{$service_center.ServiceCentreJobNo|escape:'html'}
                            {else}
                            TBA
                            {/if}
                            &nbsp;&nbsp;</b>
                            {$page['Text']['rma_no']|escape:'html'}: <b>
                    
                            {if $RMANumber}
                            {$RMANumber|escape:'html'}                    
                            {else}
                            TBA
                            {/if} 
                            </b>
                            </span>
                            <br><br>
                            {/if}
                        </label>
                    </p>
                </div>
            
                <div id="right-panel" class="span-4 last">
                    {*if $service_center.OnlineDiary eq 'Active'}
                    <div id="BookAppointmentElement" 
                         title="{$page['Text']['click_book_appointment']|escape:'html'}" 
                         style="cursor:pointer; text-align:center;" >
                        <img id="BookAppointment" 
                             src="{$_subdomain}/css/Skins/{$_theme}/images/diary_calendar.png" 
                             alt="{$page['Text']['click_book_appointment']|escape:'html'}" 
                             title="{$page['Text']['click_book_appointment']|escape:'html'}" 
                             width="68" height="84" />
                        <br>
                        <span id="BookAppointmentText" >
                        {$page['Labels']['book_appointment']|escape:'html'}
                        </span>
                    </div>                      
                    {else*}           
                    <div id="BookAppointmentElement"  style="width: 145px; height: 150px; text-align:center;" ></div>   
                    {*/if*} 
                </div>
            </div>    
        
            <p>
                <!--input type="submit" name="print_job_record_btn" id="print_job_record_btn" class="textSubmitButton" onclick="return false"  value="{$page['Buttons']['print_job_record']|escape:'html'}" -->
                <!--input type="submit" name="print_customer_receipt_btn" id="print_customer_receipt_btn" class="textSubmitButton" onclick="return false"  value="{$page['Buttons']['print_customer_receipt_btn']|escape:'html'}" -->
                <input type="submit" name="print_btn" id="print_btn" class="textSubmitButton" onclick="return false"  value="{$page['Buttons']['print_button']|escape:'html'}" >     
            </p>    
            
            <p>           
                <input type="submit" name="open_job_record_btn" id="open_job_record_btn" class="textSubmitButton leftBtn"  value="{$page['Buttons']['open_job_record']|escape:'html'}" >
                <input type="submit" name="job_booking_finish_btn" id="job_booking_finish_btn" class="textSubmitButton rightBtn"  value="{$page['Buttons']['finish']|escape:'html'}" >            
            </p>

         </fieldset> 

        
        </form>

    </div>   
        
            
      {*if $service_center.OnlineDiary eq 'Active'}
      
          
           <div id="BookAppointmentElement" title="{$page['Text']['click_book_appointment']|escape:'html'}" style="z-index:9999;cursor:pointer;width: 145px; height: 150px; position:relative; right: -750px; top: -265px;text-align:center;" >

                    <img id="BookAppointment" src="{$_subdomain}/css/Skins/{$_theme}/images/diary_calendar.png" alt="{$page['Text']['click_book_appointment']|escape:'html'}" title="{$page['Text']['click_book_appointment']|escape:'html'}" width="68" height="84" />

                    <br>
                    <span id="BookAppointmentText" >
                    {$page['Labels']['book_appointment']|escape:'html'}
                    </span>

           </div>          
             
       {else}
            
           <div id="BookAppointmentElement"  style="z-index:9999;width: 145px; height: 150px; position:relative; right: -750px; top: -265px;text-align:center;" ></div> 
           
       {/if*}    
       

        {*if $service_center.CourierStatus eq 'Active'}
      
          
            <div id="CourierAppointmentElement" title="{$page['Text']['click_courier_appointment']|escape:'html'}" style="z-index:9999;cursor:pointer;width: 145px; height: 150px; position:relative; right: -50px; top: -265px;text-align:center;" >

                    <img id="CourierAppointment" src="{$_subdomain}/css/Skins/{$_theme}/images/icons_skyline_truck_big.png" alt="{$page['Text']['click_courier_appointment']|escape:'html'}" title="{$page['Text']['click_courier_appointment']|escape:'html'}" width="68" height="53" />

                    <br>
                    <span id="CourierAppointmentText" >
                    
                    </span>


            </div>  
       {else}
           
           <div id="CourierAppointmentElement" style="z-index:9999;width: 145px; height: 150px; position:relative; right: -50px; top: -265px;text-align:center;" ></div>
           
       {/if*}       
            
{* 14/03/2013 Comment out the demo COURIER form   
              Replace with new spec form
              
   <div style="display:none;" >

        
         <div id="DivCourierAppointment" class="SystemAdminFormPanel"  >
    
                <form id="CourierPreferencesForm" name="CourierPreferencesForm" method="post"  action="#" class="inline" autocomplete="off" >

                <fieldset>
                    <legend title="" >{$page['Text']['courier_preference']|escape:'html'}</legend>

                    <p><label id="suggestText" ></label></p>
                        


                    
                         <p>
                             <label class="fieldLabel" >{$page['Labels']['collection_date']|escape:'html'}:</label>
                             <input style="width:276px;" type="text" class="text" id="CollectionDate"  name="CollectionDate" value="{$CollectionDate|escape:'html'}"   >

                         </p>
                         
                         
                         <p>
                             <label class="fieldLabel" >{$page['Labels']['preferred_courier']|escape:'html'}:</label>
                             <select  name="CourierID" id="CourierID" class="text">
                   
                                <option value="" {if $CourierID eq ''}selected="selected"{/if}   ></option>
                                {foreach from=$couriers item=courier}
                                 <option value="{$courier.CourierID}" {if $CourierID eq $courier.CourierID}selected="selected"{/if} >{$courier.AccountNo|escape:'html'}</option>
                                 {/foreach}

                             </select> 

                         </p>

                        <p>

                            <span class= "bottomButtons" >

                                
                                <input type="hidden" name="JobID" class="textSubmitButton" id="JobID"  value="{$bp_number|escape:'html'}" >
                                
                                <input type="submit" name="update_save_btn" class="textSubmitButton" id="update_save_btn"  value="{$page['Buttons']['save']|escape:'html'}" >

                               &nbsp;
                                    
                                    <input type="submit" name="cancel_btn" class="textSubmitButton" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['cancel']|escape:'html'}" >

                                    <span id="processDisplayText" style="color:red;display:none;" ><img src="{$_subdomain}/css/Skins/{$_theme}/images/loader.gif" width="16" height="16" >&nbsp;&nbsp;{$page['Text']['process_record']|escape:'html'}</span>    

                            </span>

                        </p>

                </fieldset>    

                </form>        


        </div>
        
        

    </div>
*}
                                    
    
                                    
</div>
{/block}
