# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.200');

# ---------------------------------------------------------------------- #
# Modify table "report_progress"                                         #
# ---------------------------------------------------------------------- #
ALTER TABLE `report_progress` CHANGE COLUMN `Status` `Status` ENUM('running','finished','abort') NULL DEFAULT NULL AFTER `Hash`;


# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.201');
