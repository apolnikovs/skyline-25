# ---------------------------------------------------------------------- #
# Script generated with: DeZign for Databases V7.2.0                     #
# Target DBMS:           MySQL 5                                         #
# Project file:          SkyLine.dez                                     #
# Project name:          SkyLine                                         #
# Author:                Brian Etherington                               #
# Script type:           Alter database script                           #
# Created on:            2012-11-12 17:16                                #
# ---------------------------------------------------------------------- #


# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                                     #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.121');

# ---------------------------------------------------------------------- #
# Drop foreign key constraints                                           #
# ---------------------------------------------------------------------- #

ALTER TABLE `branch` DROP FOREIGN KEY `county_TO_branch`;

ALTER TABLE `branch` DROP FOREIGN KEY `country_TO_branch`;

ALTER TABLE `branch` DROP FOREIGN KEY `user_TO_branch`;

ALTER TABLE `status` DROP FOREIGN KEY `user_TO_status`;

ALTER TABLE `ra_status` DROP FOREIGN KEY `brand_TO_ra_status`;

ALTER TABLE `ra_status` DROP FOREIGN KEY `user_TO_ra_status`;

ALTER TABLE `client_branch` DROP FOREIGN KEY `branch_TO_client_branch`;

ALTER TABLE `user` DROP FOREIGN KEY `branch_TO_user`;

ALTER TABLE `job` DROP FOREIGN KEY `branch_TO_job`;

ALTER TABLE `brand_branch` DROP FOREIGN KEY `branch_TO_brand_branch`;

ALTER TABLE `status_history` DROP FOREIGN KEY `status_TO_status_history`;

ALTER TABLE `job` DROP FOREIGN KEY `status_TO_job`;

ALTER TABLE `client` DROP FOREIGN KEY `branch_TO_client`;

ALTER TABLE `job` DROP FOREIGN KEY `ra_status_TO_job`;

ALTER TABLE `ra_history` DROP FOREIGN KEY `ra_status_TO_ra_history_2`;

# ---------------------------------------------------------------------- #
# Modify table "branch"                                                  #
# ---------------------------------------------------------------------- #

ALTER TABLE `branch` ADD COLUMN `ContactFax` VARCHAR(40);

ALTER TABLE `branch` ADD COLUMN `AccountNo` VARCHAR(40);

# ---------------------------------------------------------------------- #
# Modify table "status"                                                  #
# ---------------------------------------------------------------------- #

ALTER TABLE `status` ADD COLUMN `Colour` VARCHAR(10);

ALTER TABLE `status` MODIFY `Colour` VARCHAR(10) AFTER `StatusName`;

# ---------------------------------------------------------------------- #
# Modify table "ra_status"                                               #
# ---------------------------------------------------------------------- #

ALTER TABLE `ra_status` ADD COLUMN `Colour` VARCHAR(10);

# ---------------------------------------------------------------------- #
# Add foreign key constraints                                            #
# ---------------------------------------------------------------------- #

ALTER TABLE `branch` ADD CONSTRAINT `county_TO_branch` 
    FOREIGN KEY (`CountyID`) REFERENCES `county` (`CountyID`);

ALTER TABLE `branch` ADD CONSTRAINT `country_TO_branch` 
    FOREIGN KEY (`CountryID`) REFERENCES `country` (`CountryID`);

ALTER TABLE `branch` ADD CONSTRAINT `user_TO_branch` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `status` ADD CONSTRAINT `user_TO_status` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `ra_status` ADD CONSTRAINT `brand_TO_ra_status` 
    FOREIGN KEY (`BrandID`) REFERENCES `brand` (`BrandID`);

ALTER TABLE `ra_status` ADD CONSTRAINT `user_TO_ra_status` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `client_branch` ADD CONSTRAINT `branch_TO_client_branch` 
    FOREIGN KEY (`BranchID`) REFERENCES `branch` (`BranchID`);

ALTER TABLE `user` ADD CONSTRAINT `branch_TO_user` 
    FOREIGN KEY (`BranchID`) REFERENCES `branch` (`BranchID`);

ALTER TABLE `job` ADD CONSTRAINT `branch_TO_job` 
    FOREIGN KEY (`BranchID`) REFERENCES `branch` (`BranchID`);

ALTER TABLE `brand_branch` ADD CONSTRAINT `branch_TO_brand_branch` 
    FOREIGN KEY (`BranchID`) REFERENCES `branch` (`BranchID`);

ALTER TABLE `status_history` ADD CONSTRAINT `status_TO_status_history` 
    FOREIGN KEY (`StatusID`) REFERENCES `status` (`StatusID`);

ALTER TABLE `job` ADD CONSTRAINT `status_TO_job` 
    FOREIGN KEY (`StatusID`) REFERENCES `status` (`StatusID`);

ALTER TABLE `client` ADD CONSTRAINT `branch_TO_client` 
    FOREIGN KEY (`DefaultBranchID`) REFERENCES `branch` (`BranchID`);

ALTER TABLE `job` ADD CONSTRAINT `ra_status_TO_job` 
    FOREIGN KEY (`RAStatusID`) REFERENCES `ra_status` (`RAStatusID`);

ALTER TABLE `ra_history` ADD CONSTRAINT `ra_status_TO_ra_history_2` 
    FOREIGN KEY (`RAStatusID`) REFERENCES `ra_status` (`RAStatusID`);

# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #

insert into version (VersionNo) values ('1.122');
